use std::{borrow::Borrow, string::FromUtf8Error};

fn hex_of_bytes(bytes: &[u8]) -> String {
    let mut hex : String = String::with_capacity(bytes.len() * 2);
    for &byte in bytes {
        let word : String = format!("{:02x}", byte);
        hex.push_str(&word);
    }
    return hex;
}
pub struct AtomicWrite<'a> {
    nbytes: usize,
    thunk: Box<dyn FnMut(&mut Vec<u8>) + 'a>,
}

impl<'a> AtomicWrite<'a> {
    pub fn apply(mut self, buf: &mut Vec<u8>) {
        (self.thunk)(buf);
    }

    pub fn new(thunk: Box<dyn FnMut(&mut Vec<u8>)>, nbytes: usize) -> Self {
        Self { nbytes, thunk }
    }

    #[allow(dead_code)]
    pub fn from_word(word: u8) -> Self {
        Self {
            nbytes: 1,
            thunk: Box::new(move |buf: &mut Vec<u8>| buf.push(word)),
        }
    }

    #[allow(dead_code)]
    pub fn from_words(words: &'a [u8]) -> Self {
        Self {
            nbytes: words.len(),
            thunk: Box::new(move |buf| buf.extend_from_slice(words)),
        }
    }
}

/// Builder: Serialization Target Object Abstraction
///
/// Monoidal (through `std::ops::Add`) string-builder
/// made up of raw bytes, that can be displayed as a hexstring
/// or a raw minary string
pub trait Builder
where
    Self: std::ops::Add<Self, Output = Self> + Sized + From<Vec<u8>>,
{
    type Segment;
    type Final: Into<Vec<u8>>;

    fn promote(seg: Self::Segment) -> Self;

    /// Constructor used for instantiating builders that consist of a single 8-bit word
    fn word(b: u8) -> Self;

    /// Constructor used for instantiating builders that consist of a fixed-size array of 8-bit words
    fn words<const N: usize>(b: [u8; N]) -> Self;

    /// Consume the Builder object and return a vector of its contents
    fn into_vec(self) -> Vec<u8> {
        self.finalize().into()
    }

    /// Return a string consisting of the raw hexadecimal sequence of words in the Builder
    fn into_hex(self) -> String {
        hex_of_bytes(self.into_vec().borrow())
    }

    fn finalize(self) -> Self::Final;

    /// Return a Builder object containing zero bytes. Defaults to words over empty array.
    fn empty() -> Self {
        Self::words([])
    }
    /// Attempt to convert the Builder object into a string in binary representation
    fn into_bin(self) -> Result<String, FromUtf8Error> {
        String::from_utf8(self.into_vec())
    }

    /// Determine the length of the Builder value in bytes
    fn len(&self) -> usize;
}

pub trait TransientBuilder<'a>: Builder {
    /// Construct a Builder object from a closure that writes data to a vector
    fn delayed(aw: AtomicWrite<'a>) -> Self {
        let mut raw = Vec::new();
        aw.apply(&mut raw);
        raw.into()
    }
}

pub mod lazy;
pub mod owned;
pub mod strict;