use std::{
    borrow::Borrow,
    collections::LinkedList,
    iter::FromIterator,
    ops::{Add, AddAssign},
};

use super::TransientBuilder;

pub struct OwnedSegment(Vec<u8>);

impl OwnedSegment {
    fn promote(self) -> OwnedBuilder {
        OwnedBuilder {
            len: self.0.len(),
            chunks: LinkedList::from([self]),
        }
    }
}

pub struct OwnedBuilder {
    len: usize,
    chunks: LinkedList<OwnedSegment>,
}

impl From<u8> for OwnedSegment {
    fn from(word: u8) -> Self {
        Self(vec![word])
    }
}

impl<const N: usize> From<[u8; N]> for OwnedSegment {
    fn from(words: [u8; N]) -> Self {
        Self(words.to_vec())
    }
}

impl<const N: usize> From<&[u8; N]> for OwnedSegment {
    fn from(words: &[u8; N]) -> Self {
        Self(words.to_vec())
    }
}

impl From<&[u8]> for OwnedSegment {
    fn from(words: &[u8]) -> Self {
        Self(words.to_vec())
    }
}

impl From<Vec<u8>> for OwnedSegment {
    fn from(buf: Vec<u8>) -> Self {
        Self(buf)
    }
}

impl Into<Vec<u8>> for OwnedSegment {
    fn into(self) -> Vec<u8> {
        self.0
    }
}

impl FromIterator<u8> for OwnedSegment {
    fn from_iter<T: IntoIterator<Item = u8>>(iter: T) -> Self {
        Self(Vec::from_iter(iter))
    }
}

impl Clone for OwnedSegment {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl<T> From<T> for OwnedBuilder
where
    OwnedSegment: From<T>
{
    fn from(val: T) -> Self {
        OwnedSegment::from(val).promote()
    }
}

impl super::Builder for OwnedBuilder {
    type Segment = OwnedSegment;
    type Final = Vec<u8>;

    fn empty() -> Self {
        Self { len: 0, chunks: LinkedList::new() }
    }

    fn promote(seg: OwnedSegment) -> OwnedBuilder {
        seg.promote()
    }

    fn word(b: u8) -> Self {
        b.into()
    }

    fn words<const N: usize>(b: [u8; N]) -> Self {
        b.into()
    }

    fn finalize(self) -> Vec<u8> {
        let mut buf = Vec::with_capacity(self.len);
        for mut ele in self.chunks {
            buf.append(&mut ele.0);
        }
        buf
    }

    fn len(&self) -> usize {
        self.len
    }
}

impl TransientBuilder<'_> for OwnedBuilder {}

impl OwnedBuilder {
    pub fn empty() -> Self {
        Self { len: 0, chunks: LinkedList::new() }
    }

    pub fn push(&mut self, byte: u8) {
        self.len += 1;
        if let Some(last) = self.chunks.back_mut() {
            last.0.push(byte);
        } else {
            self.chunks.push_back(OwnedSegment::from(byte));
        }
    }

    pub fn append<T: Borrow<[u8]>>(&mut self, extra: T) {
        *self += extra;
    }
}

impl Extend<u8> for OwnedBuilder {
    fn extend<T: IntoIterator<Item = u8>>(&mut self, iter: T) {
        self.chunks.push_back(OwnedSegment::from_iter(iter));
    }
}

impl<T: Borrow<[u8]>> Add<T> for OwnedBuilder {
    type Output = Self;

    fn add(self, rhs: T) -> Self::Output {
        let slice : &[u8] = rhs.borrow();
        self + OwnedSegment::from(slice).promote()
    }
}

impl<T: Borrow<[u8]>> AddAssign<T> for OwnedBuilder {
    fn add_assign(&mut self, rhs: T) {
        *self += OwnedSegment::from(rhs.borrow()).promote()
    }
}

impl AddAssign<OwnedBuilder> for OwnedBuilder {
    fn add_assign(&mut self, mut rhs: Self) {
        self.chunks.append(&mut rhs.chunks);
    }
}

impl Add<OwnedBuilder> for OwnedBuilder {
    type Output = Self;

    fn add(self, mut rhs: Self) -> Self::Output {
        let len =  self.len + rhs.len;
        let mut chunks = self.chunks;
        chunks.append(&mut rhs.chunks);
        Self { len, chunks }
    }
}